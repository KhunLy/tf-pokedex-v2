import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PokemonsRequest } from '../models/pokemonsRequest';
import { PokemonRequest } from '../models/pokemonRequest';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private httpClient: HttpClient) { }

  getAll(url: string = 'https://pokeapi.co/api/v2/pokemon'): Observable<PokemonsRequest> {
    return this.httpClient.get<PokemonsRequest>(url);
  }

  get(url: string): Observable<PokemonRequest> {
    return this.httpClient.get<PokemonRequest>(url);
  }
}
