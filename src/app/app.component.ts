import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'tf-dataviz-angular';

  
  private _url : string;
  public get url() : string {
    return this._url;
  }
  public set url(v : string) {
    this._url = v;
  }
  

  onUrlChanged(url: string) {
    this.url = url;
  }
}
