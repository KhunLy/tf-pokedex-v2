import { Component, OnInit, Input } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';
import { PokemonRequest } from 'src/app/models/pokemonRequest';
import { ChartDataset, ChartOptions, ChartType } from 'chart.js';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  @Input()
  public set url(v : string) {
    if(v != null){
      this.isLoading = true;
      this.pokemonService.get(v).subscribe(pkReq => {
        this.model = pkReq;
        console.log(pkReq);
        this.radarChartData = [{ 
          data: pkReq.stats.map(item => {
            return item.base_stat;
          }),
          label: this.model.name,
        }];
        this.radarChartLabels = pkReq.stats.map(item => {
          return item.stat.name
        })
      }, null, () => { this.isLoading = false; })
    }
  }

  
  private _isLoading : boolean;
  public get isLoading() : boolean {
    return this._isLoading;
  }
  public set isLoading(v : boolean) {
    this._isLoading = v;
  }

  
  private _model : PokemonRequest;
  public get model() : PokemonRequest {
    return this._model;
  }
  public set model(v : PokemonRequest) {
    this._model = v;
  }



  public radarChartOptions: ChartOptions = {
    scales: {
      r: {
        min: 0,
        max: 150
      }
    } 
  };

  public radarChartLabels: string[] = [];

  public radarChartData: ChartDataset[] = [];

  public radarChartType: string = 'radar';
  
  constructor(private pokemonService: PokemonService) { }

  ngOnInit() {
  }

}
