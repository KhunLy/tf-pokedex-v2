import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { PokemonsRequest } from 'src/app/models/pokemonsRequest';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.scss']
})
export class MasterComponent implements OnInit {

  @Output() urlChanged: EventEmitter<string> = new EventEmitter<string>();

  private _model : PokemonsRequest;
  public get model() : PokemonsRequest {
    return this._model;
  }
  public set model(v : PokemonsRequest) {
    this._model = v;
  }

  
  private _isLoading : boolean;
  public get isLoading() : boolean {
    return this._isLoading;
  }
  public set isLoading(v : boolean) {
    this._isLoading = v;
  }
  

  constructor(private pokeService: PokemonService) { }

  ngOnInit() {
    this.isLoading = true;
    this.pokeService.getAll().subscribe(data => {
      this._model = data;
    },error => {}, () => this.isLoading = false);
  }

  onPrev() {
    this.isLoading = true;
    this.pokeService.getAll(this.model.previous).subscribe(data => {
      this.model = data;
    },error => {}, () => this.isLoading = false);
  }

  onNext() {
    this.isLoading = true;
    this.pokeService.getAll(this.model.next).subscribe(data => {
      this.model = data;
    },error => {}, () => this.isLoading = false);
  } 
  
  onSelect(url: string) {
    this.urlChanged.next(url);
  }
}
